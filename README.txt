Referral Links
Developed by: Steve McKenzie (steve [at] stevemckenzie.ca)
Sponsored By: HerFabLife (herfablife.com)

Installation:
- Administer -> Site building -> Modules - to install
- Administer -> User management -> Access - to set up user permissions

Functionality:
- Add referral sites at Administer -> Referral links and view stats about where referral signups are coming from.

Admin:
- Administer -> Referral links - to view referral stats and add referral sites.
